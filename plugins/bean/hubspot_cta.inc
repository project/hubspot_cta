<?php
/**
 * @file
 * Hubspot Calls-to-action bean plugin.
 */

class HubspotCallToActionBean extends BeanPlugin {
  /**
   * Declares default block settings.
   */
  public function values() {
    return array(
      'settings' => array(
        'account_id' => variable_get('hubspot_cta_account_id', ''),
        'cta_id' => '',
      ),
    );
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    $form = array();
    $form['settings'] = array(
      '#type' => 'fieldset',
      '#tree' => 1,
      '#title' => t('Options'),
    );

    $form['settings']['account_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Account ID'),
      '#description' => t('Hubspot Account ID.'),
      '#size' => 25,
      '#required' => TRUE,
      '#maxlength' => 25,
      '#element_validate' => array( 'element_validate_integer_positive' ),
      '#default_value' => isset($bean->settings['account_id']) ? $bean->settings['account_id'] : '',
    );

    $form['settings']['cta_id'] = array(
      '#type' => 'textfield',
      '#title' => t('CTA ID'),
      '#description' => t('ID of the CTA to embed.'),
      '#size' => 25,
      '#required' => TRUE,
      '#default_value' => isset($bean->settings['cta_id']) ? $bean->settings['cta_id'] : '',
    );

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $account_id = $bean->settings['account_id'];
    $cta_id = check_plain( $bean->settings['cta_id'] );
    $content['hubspot_cta']['#markup'] = hubspot_cta_render( $account_id, $cta_id );
    return $content;
  }
}
